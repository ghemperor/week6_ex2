library bignumber_util;

class Bignumber_util {
  Sum( String x, String y){
    String result="";

    if (x.length<y.length){
      while (x.length<y.length) {
        x= "0"+x;
      }
    }
    if (x.length>y.length) {
      while (x.length > y.length) {
        y = "0" + y;
      }
    }
      int remember =0;
      for ( int i = x.length-1; i>=0; i--) {
        int re= int.parse(x[i])+ int.parse(y[i]) + remember;
        if(re <=9){
          remember = 0;
        }
        else {
          remember=1;
        }
        if(i!=0){
          re = re%10;
        }
        result= re.toString()+ result;
      }
    return result;
  }
}
