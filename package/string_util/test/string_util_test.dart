import 'package:flutter_test/flutter_test.dart';

import 'package:string_util/string_util.dart';

void main() {
  test('Count words in name', () {
    final stringutil = Sring_util();
    expect(stringutil.Count_String('Dang Nhat Tuong Vy'),4);
    expect(stringutil.Count_String('Nguyen Minh Tri'),3);
    // expect(stringutil.Count_String('Dang My An'),4);
    expect(stringutil.Count_String('Nguyen Thi Phuong Thanh'),4);
  });

  test('Remove Unicode', () {
    final stringutil = Sring_util();
    expect(stringutil.Remove_Unicode('Đặng Nhật Tường Vy'),'Dang Nhat Tuong Vy');
    expect(stringutil.Remove_Unicode('Nguyễn Minh Trí'),'Nguyen Minh Tri');
    expect(stringutil.Remove_Unicode('Đặng Mỹ An'),'Dang My An');
  });
}
